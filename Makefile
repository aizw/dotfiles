LOCALBIN := ${HOME}/bin

tools: localbin symlinks

localbin:
	mkdir -p ${LOCALBIN}

symlinks:
	ln -sf ${HOME}/workspace/dotfiles/.irbrc ${HOME}/
	ln -sf ${HOME}/workspace/dotfiles/.zshrc ${HOME}/

zsh-setup:
	${HOME}/workspace/dotfiles/zsh_setup.sh

the-silver-searcher-setup:
	yes | sudo yum install the_silver_searcher
