# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
> . /etc/bashrc
fi

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions
alias rm="rm -i"
alias cp="cp -i"
alias mv="mv -i"
alias sudo="sudo env PATH=\$PATH"

export PATH=/opt/local/bin:/opt/local/sbin/:$PATH
export MANPATH=/opt/local/man:$MANPAT
export PATH=$PATH:$GOPATH/bin
