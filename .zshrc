export LANG=ja_JP.UTF-8
export SVN_EDITOR=vim
export GOPATH=$HOME/go
export PATH=$PATH:~/bin:/usr/bin/:/usr/local/bin:$GOPATH/bin:Dropbox\ \(LR\)/bin/
export MANPATH=/opt/local/man:$MANPATH

# set alias
alias vi="vim"
alias ll="ls -l"
alias rm='rm -i'
alias dif='git diff | vim -R -'
alias ag='ag -S'
alias cdc='cd $HOME/apps/lrms/current'
alias cdt='cd $HOME/apps/lrms/current_for_test'

# colors
PROMPT='%F{cyan}%/%%%f '                #カレントディレクトリを表示＆色月表示
PROMPT2='%F{cyan}%_%%%f '

SPROMPT="%r is correct? [n,y,a,e]: "
HISTFILE=$HOME/.zsh-history            # 履歴の保存先
HISTSIZE=10000                         #メモリに展開する履歴の数
SAVEHIST=100000                        #保存する履歴の数
setopt extended_history                # 履歴ファイルに時刻を記録
function history-all { history -E 1 }  # 全履歴の一覧を出力する

# C-s でインクリメンタルサーチできるように
stty stop undef

# history incremental search
bindkey "^R" history-incremental-pattern-search-backward
bindkey "^S" history-incremental-pattern-search-forward

# 補完機能
autoload -U compinit
compinit
zstyle ':completion:*:default' menu select=1

# ディレクトリの移動履歴の利用
setopt auto_pushd

# --prefix=/usr などの = 以降も補完
setopt magic_equal_subst

# ディレクトリ名だけをうつ cd実行される
setopt auto_cd

setopt correct
setopt nobeep

# 同じディレクトリを pushd しない
setopt pushd_ignore_dups

# =command を command のパス名に展開する
setopt equals

# カッコの対応などを自動的に補完
setopt auto_param_keys

# 直前と同じコマンドをヒストリに追加しない
setopt hist_ignore_dups

# Ctrl+D では終了しないようになる（exit, logout などを使う）
setopt ignore_eof

# 自動補完
# http://d.hatena.ne.jp/lurker/20061205/1165246310
autoload predict-on
zle -N predict-on
zle -N predict-off
bindkey '^X^Q' predict-on
bindkey '^Q' predict-off
zstyle ':predict' verbose true

## 補完機能の強化
autoload -Uz compinit
compinit

# コアダンプサイズを制限
limit coredumpsize 102400
# 出力の文字列末尾に改行コードが無い場合でも表示
unsetopt promptcr
# Emacsライクキーバインド設定
bindkey -e

# 色を使う
setopt prompt_subst
# ビープを鳴らさない
setopt nobeep
# 内部コマンド jobs の出力をデフォルトで jobs -l にする
setopt long_list_jobs
# 補完候補一覧でファイルの種別をマーク表示
setopt list_types
# サスペンド中のプロセスと同じコマンド名を実行した場合はリジューム
setopt auto_resume
# 補完候補を一覧表示
setopt auto_list
# 直前と同じコマンドをヒストリに追加しない
setopt hist_ignore_dups
# cd 時に自動で push
setopt auto_pushd
# 同じディレクトリを pushd しない
setopt pushd_ignore_dups
# ファイル名で #, ~, ^ の 3 文字を正規表現として扱う
setopt extended_glob
# TAB で順に補完候補を切り替える
setopt auto_menu
# zsh の開始, 終了時刻をヒストリファイルに書き込む
setopt extended_history
# =command を command のパス名に展開する
setopt equals
# --prefix=/usr などの = 以降も補完
setopt magic_equal_subst
# ヒストリを呼び出してから実行する間に一旦編集
setopt hist_verify
# ファイル名の展開で辞書順ではなく数値的にソート
setopt numeric_glob_sort
# 出力時8ビットを通す
setopt print_eight_bit
# ヒストリを共有
setopt share_history
# 補完候補のカーソル選択を有効に
zstyle ':completion:*:default' menu select=1
# 補完候補の色づけ
eval `dircolors`
export ZLS_COLORS=$LS_COLORS
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
# カッコの対応などを自動的に補完
setopt auto_param_keys
# ディレクトリ名の補完で末尾の / を自動的に付加し、次の補完に備える
setopt auto_param_slash
# スペルチェック
setopt correct
# {a-c} を a b c に展開する機能を使えるようにする
setopt brace_ccl
# Ctrl+S/Ctrl+Q によるフロー制御を使わないようにする
setopt NO_flow_control
# コマンドラインの先頭がスペースで始まる場合ヒストリに追加しない
setopt hist_ignore_space
# コマンドラインでも # 以降をコメントと見なす
setopt interactive_comments
# ファイル名の展開でディレクトリにマッチした場合末尾に / を付加する
setopt mark_dirs
# 補完候補を詰めて表示
setopt list_packed
# 最後のスラッシュを自動的に削除しない
setopt noautoremoveslash

# http://d.hatena.ne.jp/hiboma/20120315/1331821642
pbcopy-buffer(){
  print -rn $BUFFER | pbcopy
  zle -M "pbcopy: ${BUFFER}"
}

zle -N pbcopy-buffer
bindkey '^x^p' pbcopy-buffer

if which pbcopy >/dev/null 2>&1 ; then
    # Mac
    alias -g C='| pbcopy'
elif which xsel >/dev/null 2>&1 ; then
    # Linux
    alias -g C='| xsel --input --clipboard'
elif which putclip >/dev/null 2>&1 ; then
    # Cygwin
    alias -g C='| putclip'
fi


###########
# show current branch
#
# @see
# http://stackoverflow.com/questions/1128496/to-get-a-prompt-which-indicates-git-branch-in-zsh

setopt prompt_subst
autoload -Uz vcs_info
zstyle ':vcs_info:*' actionformats \
    '%F{5}(%f%s%F{5})%F{3}-%F{5}[%F{2}%b%F{3}|%F{1}%a%F{5}]%f '
zstyle ':vcs_info:*' formats       \
    '%F{5}(%f%s%F{5})%F{3}-%F{5}[%F{2}%b%F{5}]%f '
zstyle ':vcs_info:(sv[nk]|bzr):*' branchformat '%b%F{1}:%F{3}%r'

zstyle ':vcs_info:*' enable git cvs svn

# or use pre_cmd, see man zshcontrib
vcs_info_wrapper() {
  vcs_info
  if [ -n "$vcs_info_msg_0_" ]; then
    echo "%{$fg[grey]%}${vcs_info_msg_0_}%{$reset_color%}$del"
  fi
}
RPROMPT=$'$(vcs_info_wrapper)'

# rbenvにパスを通す
[[ -d ~/.rbenv  ]] && \
  export PATH=${HOME}/.rbenv/bin:${PATH} && \
  eval "$(rbenv init -)"
